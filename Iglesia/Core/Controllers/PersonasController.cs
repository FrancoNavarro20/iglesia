﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Core.Data;
using Microsoft.AspNetCore.Http;
using Core.Models;
using Core.Services;

namespace Core.Controllers
{
    public class PersonasController : BaseController
    {
        private readonly ApplicationDbContext _context;
        private readonly IHttpContextAccessor _httpContextAccesor;
        public PersonasController(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccesor = httpContextAccessor;
        }

        // GET: Personas
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult _ListadoDePersonas(string Id)
        {
            List<Persona> personas = _context.Persona.ToList();
            //ViewBag.BapinId = Id;
            return PartialView("_ListadoDePersonas", personas);
        }

        // GET: Personas/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var persona = await _context.Persona
                .FirstOrDefaultAsync(m => m.Id == id);
            if (persona == null)
            {
                return NotFound();
            }

            return View(persona);
        }

        // GET: Personas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Personas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create(Persona persona, IFormFile file)
        {
            ModelState.Remove("Perfil");
            if (ModelState.IsValid)
            {
                persona.Perfil = ImagenService.ConvertirImagen(file);
                _context.Add(persona);
                await _context.SaveChangesAsync();
                AddPageAlerts(PageAlertType.Success, "La persona se creo correctamente .");
                return RedirectToAction(nameof(Index));
            }
            return View(persona);
        }

        // GET: Personas/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var persona = await _context.Persona.FindAsync(id);
            if (persona == null)
            {
                return NotFound();
            }
            return View(persona);
        }

        // POST: Personas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Edit(Persona persona, IFormFile file)
        {
            ModelState.Remove("Perfil");
            if (ModelState.IsValid)
            {
                try
                {
                    persona.Perfil = ImagenService.ConvertirImagen(file);
                    _context.Update(persona);
                    await _context.SaveChangesAsync();
                    AddPageAlerts(PageAlertType.Success, "La persona se modifico correctamente .");
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PersonaExists(persona.Id))
                    {
                        AddPageAlerts(PageAlertType.Error,"Ocurrio un error al guardar los datos , intentelo nuevamente mas tarde.");
                        return RedirectToAction(nameof(Index));
                    }
                }
            }
            return View(persona);
        }

        public IActionResult DetallePersona(string id)
        {
            if(id == null)
            {
                return NotFound();
            }

            var persona = _context.Persona.Find(id);
            return View("DetallePersona", persona);
        }

        public IActionResult VerFotoDePerfil(string Id)
        {
            Persona personaFoto = _context.Persona.Where(x => x.Id == Id).FirstOrDefault();
            if (personaFoto.Perfil != null)
            {
                ViewBag.Imagen = "data:image/jpeg;base64," + Convert.ToBase64String(personaFoto.Perfil);
                ViewBag.Mostrar = true;
                return PartialView("VerFotoDePerfil");
            }
            ViewBag.Mostrar = false;
            return PartialView("VerFotoDePerfil", personaFoto);
        }


        public async Task<IActionResult> DeletePersona(string id)
        {
            if(id == null)
            {
                AddPageAlerts(PageAlertType.Error, "Ha ocurrido un error al borrar la persona , intentelo nuevamente mas tarde .");
                return RedirectToAction(nameof(Index));
            }
            var persona = await _context.Persona.FindAsync(id);
            _context.Persona.Remove(persona);
            await _context.SaveChangesAsync();
            AddPageAlerts(PageAlertType.Success, "La persona se borro correctamente");
            return RedirectToAction(nameof(Index));

        }

        private bool PersonaExists(string id)
        {
            return _context.Persona.Any(e => e.Id == id);
        }
    }
}
