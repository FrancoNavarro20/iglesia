﻿using Commons.Extensions;
using Commons.Helpers;
using Commons.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Commons.Controllers.ViewComponents.Layout
{
    public class SidebarViewComponent : ViewComponent
    {
        public SidebarViewComponent()
        {
        }
        public IViewComponentResult Invoke(string filter)
        {
            System.Security.Principal.IPrincipal currentUser = this.User;

            var sidebars = new List<SidebarMenu>();
            sidebars.Add(MenuHelpers.AddHeader("Menú Principal"));
            var procesos = MenuHelpers.AddTree("Datos", "fa fa-cubes");
            var gestion = MenuHelpers.AddTree("Gestion", "fa fa-book");
            var finanzas = MenuHelpers.AddTree("Finanzas", "fa fa-book");
            //var reportes = MenuHelpers.AddTree("Reportes", "fa fa-print");
            //var sesion = MenuHelpers.AddTree("Sesion", "fa fa-gear");
            var seguridad = MenuHelpers.AddTree("Sistema", "fa fa-gear");
      
                procesos.TreeChild = new List<SidebarMenu>()
                    {
                        MenuHelpers.AddModule("Personas", "/Personas/Index", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("Noticias", "/Noticias/Index", "fa fa-circle-o text-aqua"),
                    };

                gestion.TreeChild = new List<SidebarMenu>()
                    {
                        MenuHelpers.AddModule("Actividades", "/Actividades/Index", "fa fa-circle-o text-yellow"),
                        MenuHelpers.AddModule("Turnos", "/Turnos/Index", "fa fa-circle-o text-aqua")
                    };

                //reportes.TreeChild = new List<SidebarMenu>()
                //    {
                //        MenuHelpers.AddModule("Matrices", "/ReportesMatrices/", "fa fa-circle-o text-red"),
                //        MenuHelpers.AddModule("Diccionarios", "/ReportesDiccionarios/", "fa fa-circle-o text-red"),
                //    };

                //sesion.TreeChild = new List<SidebarMenu>()
                //{
                //    MenuHelpers.AddModule("Registra Usuario", "/Identity/Account/Register", "fa fa-user-secret"),
                //    MenuHelpers.AddModule("Cierra Sesion", "/Identity/Account/Logout", "fa fa-sign-out"),
                //};
            // Finanzas
            finanzas.TreeChild = new List<SidebarMenu>();
                finanzas.TreeChild.Add(MenuHelpers.AddModule("Compras", "/Admin/Usuario", "fa fa-circle-o text-green"));
                finanzas.TreeChild.Add(MenuHelpers.AddModule("Gastos mensuales", "/SecurityRoles", "fa fa-circle-o text-green"));
                finanzas.TreeChild.Add(MenuHelpers.AddModule("Ingresos", "/SecurityFunctions", "fa fa-circle-o text-green"));
                finanzas.TreeChild.Add(MenuHelpers.AddModule("Pagos", "/SecurityFunctions", "fa fa-circle-o text-green"));


            // SEGURIDAD
            seguridad.TreeChild = new List<SidebarMenu>();
                    seguridad.TreeChild.Add(MenuHelpers.AddModule("Usuarios", "/Usuario/Index", "fa fa-circle-o text-red"));
                    seguridad.TreeChild.Add(MenuHelpers.AddModule("Roles", "/SecurityRoles", "fa fa-circle-o text-red"));
                    seguridad.TreeChild.Add(MenuHelpers.AddModule("Funciones", "/SecurityFunctions", "fa fa-circle-o text-red"));
           
            sidebars.Add(procesos);
            sidebars.Add(gestion);
            sidebars.Add(finanzas);
            sidebars.Add(seguridad);
            //sidebars.Add(reportes);
            //sidebars.Add(sesion);
            return View("LayoutSidebar", sidebars);

        }
    }
}
