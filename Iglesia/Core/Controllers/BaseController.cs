﻿using Commons.Controllers;
using Commons.Identity.Services;
using Core.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Controllers
{
    public class BaseController : CommonsController
    {
        public ApplicationDbContext Context;
        private IHttpContextAccessor _contextAccessor;
        private IHostingEnvironment _env;
        public readonly SignInService<Usuario> _signInService;
        public BaseController()
        {
                
        }
        public BaseController(ApplicationDbContext context, IHostingEnvironment env)
        {
            Context = context;
            _env = env;
        }
        public BaseController(ApplicationDbContext context, SignInService<Usuario> signService, IHttpContextAccessor contextAccessor)
        {
            _signInService = signService;
            Context = context;
            _contextAccessor = contextAccessor;
        }

        public BaseController(ApplicationDbContext context)
        {
            Context = context;
        }
    }
}
