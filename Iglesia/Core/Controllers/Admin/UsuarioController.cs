﻿using Commons.Extensions;
using Commons.Identity.Extensions;
using Commons.Identity.Services;
using Commons.Models;
using Core.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Core.Controllers.Admin
{
    public class UsuarioController : BaseController
    {
        private readonly ApplicationDbContext _context;
        private UserManager<Usuario> _userManager;
        private SignInManager<Usuario> _signIn;
        private UserService<Usuario> _claimsService;

        //public string _Uat { get; }

        public UsuarioController(ApplicationDbContext context, IHttpContextAccessor contextAccessor, UserManager<Usuario> userManager, SignInManager<Usuario> signIn, UserService<Usuario> claimsService, SignInService<Usuario> signService) : base(context, signService, contextAccessor)
        {
            //if (_Uat != null)
            //{                
            _context = context;
            _userManager = userManager;
            _signIn = signIn;
            _claimsService = claimsService;
            //}
            //else
            //{
            //    _signInService.SignOutAsync();
            //}                         
        }



        public IActionResult Index()
        {
            if (((ClaimsPrincipal)User).IsAdmin() || HttpContext.UserHasRoute("/User/index"))
            {
                return View();
            }
            else
            {
                return Redirect("/Identity/Account/AccessDenied"); ;
            }
        }

        public IActionResult _TablaUsuarios(Page<Usuario> pageUser)
        {
            pageUser.SelectPage("/Usuario/_TablaUsuario",
                 _context.Users.Where(x => (x.Persona.Apellido.Contains(pageUser.SearchText)))
                 );

            return PartialView("_TablaUsuarios", pageUser);

        }
    }
}
