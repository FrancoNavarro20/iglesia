﻿using Core.Data;
using Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            if (!User.Identity.IsAuthenticated){ return Redirect("/Identity/Account/Login"); }
            ViewBag.Actividades = _context.Actividades.Count();
            List <Actividades> actividades = _context.Actividades.ToList();
            return View("Index", actividades);
        }

        public IActionResult FindAllEvents()
        {
            var events = _context.Actividades.Select(x => new
            {
                title = x.Descripcion,
                description = x.Observaciones,
                start = x.FechaInicio.ToString("yyyy/MM/dd"),
                end = x.FechaFinalizado.ToString("yyyy/MM/dd"),
                color = x.Color
            }).ToList();

            return new JsonResult(events);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
