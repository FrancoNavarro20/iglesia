﻿using Core.Data;
using Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Controllers
{
    public class TurnosController : BaseController
    {
        private readonly ApplicationDbContext _context;
        public TurnosController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            ViewBag.Actividades = _context.Actividades.Select(g => new SelectListItem() { Text = g.Descripcion, Value = g.Id.ToString() });
            return View();
        }

        public IActionResult _ListadoDeActividades(int? actividadId)
        {
            List<Actividades> actividades = new List<Actividades>();
            if (actividadId != 0) 
            {
                actividades = _context.Actividades.Where(x => x.Id == actividadId).ToList();
                return PartialView("_ListadoDeActividades", actividades);
            }
            actividades = _context.Actividades.ToList();
            //ViewBag.BapinId = Id;
            return PartialView("_ListadoDeActividades", actividades);
        }
    }
}
