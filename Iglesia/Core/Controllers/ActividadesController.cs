﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Core.Data;
using Core.Models;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace Core.Controllers
{
    public class ActividadesController : BaseController
    {
        private readonly ApplicationDbContext _context;
        private readonly IHttpContextAccessor _httpContextAccesor;
        public ActividadesController(ApplicationDbContext context, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _httpContextAccesor = httpContextAccessor;
        }

        #region Actividades

        public async Task<IActionResult> Index()
        {
            return View(await _context.Actividades.ToListAsync());
        }


        public IActionResult _ListadoDeActividades(string Id)
        {
            List<Actividades> actividades = _context.Actividades.ToList();
            //ViewBag.BapinId = Id;
            return PartialView("_ListadoDeActividades", actividades);
        }
        // GET: Actividades/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var actividades = await _context.Actividades
                .FirstOrDefaultAsync(m => m.Id == id);
            if (actividades == null)
            {
                return NotFound();
            }

            return View(actividades);
        }

        // GET: Actividades/Create
        public IActionResult Create()
        {
            return PartialView();
        }

        // POST: Actividades/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Actividades actividades, string color)
        {
            ModelState.Remove("Id");
            ModelState.Remove("Imagen");
            ModelState.Remove("ListaPersonas");
            if (ModelState.IsValid)
            {
                actividades.Color = color;
                _context.Add(actividades);
                await _context.SaveChangesAsync();
                AddPageAlerts(PageAlertType.Success, "La informacion de la actividad se creo correctamente .");
                return RedirectToAction(nameof(Index));
            }
            AddPageAlerts(PageAlertType.Error, "Ocurrio un error al crear la informacion de la actividad, intente nuevamente .");
            return RedirectToAction(nameof(Index));
        }

        public ActionResult _CargaArchivo(int Id)
        {
            ViewBag.ActividadId = Id;
            return PartialView();
        }
        [HttpPost]
        public async Task<ActionResult> _CargaArchivo(IFormFile file, int  ActividadId)
        {
            try
            {
                if (file == null || file.ContentType != "image/jpeg")
                {
                    AddPageAlerts(PageAlertType.Error, "Debe subir un Icono con la extensión JPG");
                }
                else
                {
                    Actividades actividades = await _context.Actividades.FindAsync(ActividadId);
                    using (var memoryStream = new MemoryStream())
                    {
                        await file.CopyToAsync(memoryStream);
                        actividades.Imagen = memoryStream.ToArray();
                        await _context.SaveChangesAsync();
                    }
                    AddPageAlerts(PageAlertType.Success, $"Se agrego correctamente la imagen de la actividad {actividades.Descripcion} .");
                }
                return RedirectToAction(nameof(Index));
            }
            catch (System.Exception)
            {
                AddPageAlerts(PageAlertType.Error, "Hubo un Error al subir la imagen. Intentelo nuevamente mas tarde.");
                return RedirectToAction("Index");
            }
        }
        public IActionResult ViewFile(int Id)
        {
            Actividades actividades = _context.Actividades.Where(x => x.Id == Id).FirstOrDefault();
            if (actividades.Imagen != null)
            {
                ViewBag.Imagen = "data:image/jpeg;base64," + Convert.ToBase64String(actividades.Imagen);
                return PartialView("VerImagen");
            }
            AddPageAlerts(PageAlertType.Error, "Hubo un Error al ver el archivo PDF. Intentelo nuevamente mas tarde.");
            return RedirectToAction("DetalleComision");
        }
        // GET: Actividades/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var actividades = await _context.Actividades.FindAsync(id);
            if (actividades == null)
            {
                return NotFound();
            }
            return PartialView(actividades);
        }

        // POST: Actividades/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Actividades actividades, string color)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    actividades.Color = color;
                    _context.Update(actividades);
                    AddPageAlerts(PageAlertType.Success, "La edicion de la informacion de la actividad se guardo correctamente .");
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ActividadesExists(actividades.Id))
                    {
                        AddPageAlerts(PageAlertType.Error,"Ha ocurrido un error al editar la actividad, intentelo nuevamente.");
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return PartialView(actividades);
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var actividades = await _context.Actividades.FindAsync(id);
                _context.Actividades.Remove(actividades);
                await _context.SaveChangesAsync();
                AddPageAlerts(PageAlertType.Success, "La actividad se borro correctamente .");
                return RedirectToAction(nameof(Index));
            }
            catch (Exception)
            {
                AddPageAlerts(PageAlertType.Error, "Hubo un error al eliminar la actividad , intentelo nuevamente .");
                return RedirectToAction(nameof(Index));
            }
          
        }

        private bool ActividadesExists(int id)
        {
            return _context.Actividades.Any(e => e.Id == id);
        }

        public IActionResult InformacionActividades(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            Actividades actividades = _context.Actividades.Find(id);
            return View(actividades);
        }


        #endregion
    }
}
