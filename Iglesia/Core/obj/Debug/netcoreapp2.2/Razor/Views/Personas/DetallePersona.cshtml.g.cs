#pragma checksum "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e6c9b1daf1da777b0534eee466cefe9656e4a39a"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Personas_DetallePersona), @"mvc.1.0.view", @"/Views/Personas/DetallePersona.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Personas/DetallePersona.cshtml", typeof(AspNetCore.Views_Personas_DetallePersona))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Franco\source\repos\Iglesia\Core\Views\_ViewImports.cshtml"
using Core;

#line default
#line hidden
#line 2 "C:\Users\Franco\source\repos\Iglesia\Core\Views\_ViewImports.cshtml"
using Core.Models;

#line default
#line hidden
#line 2 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
using Core.Services;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e6c9b1daf1da777b0534eee466cefe9656e4a39a", @"/Views/Personas/DetallePersona.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"09c0280052e18d49d7235a1cb8b4c4bcce0f5c01", @"/Views/_ViewImports.cshtml")]
    public class Views_Personas_DetallePersona : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Core.Data.Persona>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("_ListadoDeActividades"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Commons.TagHelpers.LoadTagHelper __Commons_TagHelpers_LoadTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
  
    ViewData["Title"] = "DetallePersona";

#line default
#line hidden
            BeginContext(98, 483, true);
            WriteLiteral(@"
<div class=""row"">
    <div class=""col-md-4"">
        <div class=""box box-info"">
            <div class=""box-body"">
                <div class=""container-fluid"">
                    <div class=""row"">
                        <div class=""col-md-12 titulo-detalle""><strong><i class=""fa fa-user margin-r-5""></i> Foto de perfil </strong></div>
                        <div class=""col-md-12"">
                            <p> <img class=""profile-user-img img-responsive img-circle""");
            EndContext();
            BeginWriteAttribute("src", " src=\"", 581, "\"", 629, 1);
#line 15 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
WriteAttributeValue("", 587, ImagenService.ObtenerImagen(Model.Perfil), 587, 42, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(630, 79, true);
            WriteLiteral(" style=\"width:35%;\" alt=\"Foto de perfil\"></p>\r\n                        </div>\r\n");
            EndContext();
            BeginContext(1695, 601, true);
            WriteLiteral(@"                    </div>
                    <div class=""row"">
                        <div class=""col-md-10""><strong><i class=""fa fa-map-marker margin-r-5""></i> Datos personales </strong></div>
                        <div class=""col-md-2"">
                        </div>
                        <br>
                        <div class=""col-md-12"">
                            <p class=""text-muted"">
                            </p><div class=""row"">
                                <div class=""col-md-12"">
                                    <span>Nombre y apellido <a class=""pull-right"">");
            EndContext();
            BeginContext(2297, 12, false);
#line 45 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
                                                                             Write(Model.Nombre);

#line default
#line hidden
            EndContext();
            BeginContext(2309, 2, true);
            WriteLiteral("  ");
            EndContext();
            BeginContext(2312, 14, false);
#line 45 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
                                                                                            Write(Model.Apellido);

#line default
#line hidden
            EndContext();
            BeginContext(2326, 197, true);
            WriteLiteral("</a></span><br>\r\n                                </div>\r\n                                <div class=\"col-md-12\">\r\n                                    <span>Fecha de nacimiento<a class=\"pull-right\">");
            EndContext();
            BeginContext(2524, 41, false);
#line 48 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
                                                                              Write(Model.FechaNacimiento.ToShortDateString());

#line default
#line hidden
            EndContext();
            BeginContext(2565, 179, true);
            WriteLiteral("</a></span>\r\n                                </div>\r\n                                <div class=\"col-md-12\">\r\n                                    <span>Edad <a class=\"pull-right\">");
            EndContext();
            BeginContext(2745, 10, false);
#line 51 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
                                                                Write(Model.Edad);

#line default
#line hidden
            EndContext();
            BeginContext(2755, 798, true);
            WriteLiteral(@"</a></span>
                                </div>
                            </div>
                            <p></p>
                        </div>
                    </div>

                    <hr class=""separador-detalle"">

                    <div class=""row"">
                        <div class=""col-md-8""><strong><i class=""fa fa-book margin-r-5""></i>Número de contacto</strong></div>
                        <div class=""col-md-4"">
                        </div>
                        <br>
                        <div class=""col-md-12"">
                            <p class=""text-muted"">
                            </p><div class=""row"">
                                <div class=""col-md-12"">
                                    <span>Telefono<a class=""pull-right"">");
            EndContext();
            BeginContext(3554, 14, false);
#line 69 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
                                                                   Write(Model.Telefono);

#line default
#line hidden
            EndContext();
            BeginContext(3568, 813, true);
            WriteLiteral(@"</a></span><br>
                                </div>
                            </div>
                            <p></p>
                        </div>
                    </div>
                    <hr class=""separador-detalle"">

                    <div class=""row"">
                        <div class=""col-md-10""><strong><i class=""fa fa-map-marker margin-r-5""></i> Domicilio </strong></div>
                        <div class=""col-md-2"">
                        </div>
                        <br>
                        <div class=""col-md-12"">
                            <p class=""text-muted"">
                            </p><div class=""row"">
                                <div class=""col-md-12"">
                                    <span>Calle y Altura <a class=""pull-right"">Calle ");
            EndContext();
            BeginContext(4382, 11, false);
#line 86 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
                                                                                Write(Model.Calle);

#line default
#line hidden
            EndContext();
            BeginContext(4393, 2, true);
            WriteLiteral("  ");
            EndContext();
            BeginContext(4396, 14, false);
#line 86 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
                                                                                              Write(Model.NroCalle);

#line default
#line hidden
            EndContext();
            BeginContext(4410, 384, true);
            WriteLiteral(@"</a></span><br>
                                </div>
                                <div class=""col-md-12"">
                                    <span>Provincia <a class=""pull-right"">Buenos Aires</a></span>
                                </div>
                                <div class=""col-md-12"">
                                    <span>Localidad <a class=""pull-right"">");
            EndContext();
            BeginContext(4795, 15, false);
#line 92 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
                                                                     Write(Model.Localidad);

#line default
#line hidden
            EndContext();
            BeginContext(4810, 458, true);
            WriteLiteral(@"</a></span>
                                </div>
                            </div>
                            <p></p>
                        </div>
                    </div>
                    <hr class=""separador-detalle"">
                </div>
            </div>
        </div>
    </div>
    <div class=""col-md-8"">
        <div class=""nav-tabs-custom"" style=""cursor: move;"">
            <ul class=""nav nav-tabs  ui-sortable-handle"">
");
            EndContext();
            BeginContext(5656, 76, true);
            WriteLiteral("                <li class=\"active\" id=\"actividadesLi\"><a href=\"#Actividades\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 5732, "\"", 5742, 0);
            EndWriteAttribute();
            BeginContext(5743, 127, true);
            WriteLiteral(" data-toggle=\"tab\" aria-expanded=\"true\">Actividades pendientes &nbsp; <i class=\"fa fa-users\"></i></a></li>\r\n                <li");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 5870, "\"", 5878, 0);
            EndWriteAttribute();
            BeginContext(5879, 48, true);
            WriteLiteral(" id=\"actividadesHisLi\"><a href=\"#ActividadesHis\"");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 5927, "\"", 5937, 0);
            EndWriteAttribute();
            BeginContext(5938, 296, true);
            WriteLiteral(@" data-toggle=""tab"" aria-expanded=""true"">Actividades históricas &nbsp; <i class=""fa fa-users""></i></a></li>
            </ul>
            <div class=""tab-content"">
                <div class=""tab-pane active"" id=""Actividades"" style=""position: relative; max-height: 100%;"">
                    ");
            EndContext();
            BeginContext(6234, 118, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("load", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e6c9b1daf1da777b0534eee466cefe9656e4a39a13087", async() => {
                BeginContext(6323, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
            }
            );
            __Commons_TagHelpers_LoadTagHelper = CreateTagHelper<global::Commons.TagHelpers.LoadTagHelper>();
            __tagHelperExecutionContext.Add(__Commons_TagHelpers_LoadTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            BeginWriteTagHelperAttribute();
            WriteLiteral("/Actividades/_ListadoDeActividades/");
#line 113 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
                                                                                      WriteLiteral(Model.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Commons_TagHelpers_LoadTagHelper.Url = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("load-url", __Commons_TagHelpers_LoadTagHelper.Url, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6352, 152, true);
            WriteLiteral("\r\n                </div>\r\n                <div class=\"tab-pane\" id=\"ActividadesHis\" style=\"position: relative; max-height: 100%;\">\r\n                    ");
            EndContext();
            BeginContext(6504, 118, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("load", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e6c9b1daf1da777b0534eee466cefe9656e4a39a15172", async() => {
                BeginContext(6593, 22, true);
                WriteLiteral("\r\n                    ");
                EndContext();
            }
            );
            __Commons_TagHelpers_LoadTagHelper = CreateTagHelper<global::Commons.TagHelpers.LoadTagHelper>();
            __tagHelperExecutionContext.Add(__Commons_TagHelpers_LoadTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            BeginWriteTagHelperAttribute();
            WriteLiteral("/Actividades/_ListadoDeActividades/");
#line 117 "C:\Users\Franco\source\repos\Iglesia\Core\Views\Personas\DetallePersona.cshtml"
                                                                                      WriteLiteral(Model.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Commons_TagHelpers_LoadTagHelper.Url = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("load-url", __Commons_TagHelpers_LoadTagHelper.Url, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6622, 80, true);
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Core.Data.Persona> Html { get; private set; }
    }
}
#pragma warning restore 1591
