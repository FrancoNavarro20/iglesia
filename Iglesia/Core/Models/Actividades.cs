﻿using Core.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models
{
    public class Actividades
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "El nombre de la actividad es requerida .")]
        public string Descripcion { get; set; }
        [Required(ErrorMessage = "La observacion es requerida .")]
        public string Observaciones { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de Inicio")]
        [Required(ErrorMessage = "La fecha de inicio es requerida .")]
        public DateTime FechaInicio { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Fecha de finalizacion")]
        [Required(ErrorMessage = "La Fecha de finalizacion es requerida .")]
        public DateTime FechaFinalizado { get; set; }
        [Required(ErrorMessage ="El color es requerido")]
        public string Color { get; set; }
        public byte[] Imagen { get; set; }
        public virtual List<Persona> ListaPersonas { get; set; }
        public int CalculoDias(DateTime fecha, int cantidadDias)
        {
            DateTime fechaFinalizada = fecha.AddDays(cantidadDias);
            var calculoDias = fechaFinalizada - DateTime.Now;
            int dias = calculoDias.Days + 1;
            if (dias == 0) { dias = 1; }
            else if (DateTime.Now.ToShortDateString() == fechaFinalizada.ToShortDateString()) { dias = 0; }
            else { dias = calculoDias.Days + 1; }
            return dias;
        }
    }
}
