﻿using Commons.Models;
using Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Data
{
    public class Persona : Documental
    {
        [Required(ErrorMessage = "El nombre es requerido")]
        public string Nombre { set; get; }
        [Required(ErrorMessage = "El apellido es requerido")]
        public string Apellido { set; get; }
        [Required(ErrorMessage = "El DNI es requerido")]
        public int Dni { set; get; }
        [Required(ErrorMessage = "La edad es requerida")]
        public int Edad { set; get; }
        [DataType(DataType.Date)]
        [Display(Name = "Fecha")]
        public DateTime FechaNacimiento { get; set; }
        [Required(ErrorMessage = "El teléfono es requerido")]
        public string Telefono { get; set; }
        [Required(ErrorMessage = "La localidad es requerida")]
        public string Localidad { get; set; }
        [Required(ErrorMessage = "El nombre es requerido")]
        public string Calle { get; set; }
        [Required(ErrorMessage = "La calle es requerida")]
        public int NroCalle { get; set; }
        public byte[] Perfil { get; set; }
        public virtual Actividades Actividades { get; set; }

    }
}
