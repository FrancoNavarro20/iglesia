﻿using Commons.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Data
{
    public class Usuario : CommonsUser
    {
        public virtual Persona Persona { get; set; }
        public override string GetFirstName()
        {
            return "Primer nombre";
        }
        public override string GetMiddleName()
        {
            return "Grado";
        }

        public override string GetLastName()
        {
            return "Apellido";
        }


        public override string GetRoleString()
        {
            return "Usuario";
        }

        public virtual string GetGrado()
        {
            return "rol";
        }

        public override List<IWorkSpace> GetRelatedIWorkSpaces()
        {
            return new List<IWorkSpace>();
        }
    }
}
