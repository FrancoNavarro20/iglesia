﻿using Commons.Controllers;
using Core.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Services
{
    public static class ImagenService 
    {
        public static string ObtenerImagen(byte[] img)
        {
            var imgSrc = "https://static.thenounproject.com/png/340719-200.png";
            if (img != null)
            {
                var base64 = "data:image/jpeg;base64," + Convert.ToBase64String(img);
                imgSrc = base64;
            }
            return imgSrc;
        }

        public static byte [] ConvertirImagen(IFormFile file)
        {
            byte[] perfil = null;
            try
            {
                if (file == null || file.ContentType != "image/jpeg")
                {
                    return perfil;
                }
                else
                {
                    using (var memoryStream = new MemoryStream())
                    {
                        file.CopyToAsync(memoryStream);
                        perfil = memoryStream.ToArray();
                        return perfil;
                    }
                }
            }
            catch (System.Exception)
            {
                return perfil;
            }
        }

    }
}
