﻿using Commons.Helpers;
using Commons.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Commons.Controllers.ViewComponents.Layout
{
    public class SidebarViewComponent : ViewComponent
    {
        public SidebarViewComponent()
        {
        }
        public IViewComponentResult Invoke(string filter)
        {
            System.Security.Principal.IPrincipal currentUser = this.User;

            var sidebars = new List<SidebarMenu>();
            sidebars.Add(MenuHelpers.AddHeader("Menú Principal"));
            var procesos = MenuHelpers.AddTree("Datos", "fa fa-cubes");
            var gestion = MenuHelpers.AddTree("Gestion", "fa fa-book");
            var reportes = MenuHelpers.AddTree("Reportes", "fa fa-print");
            var sesion = MenuHelpers.AddTree("Sesion", "fa fa-gear");
            if (currentUser.IsInRole("Administrador"))
            {
                procesos.TreeChild = new List<SidebarMenu>()
                    {
                        MenuHelpers.AddModule("Área tramites", "/AreaTramite/Index", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("Armas", "/Armas/", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("Perfiles", "/Perfiles/", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("Matrices", "/DatoMatrices/", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("Diccionarios", "/Diccionario/", "fa fa-circle-o text-aqua"),
                    };

                gestion.TreeChild = new List<SidebarMenu>()
                    {
                        MenuHelpers.AddModule("SDASDAS", "/Matrices/", "fa fa-circle-o text-yellow"),
                    };

                reportes.TreeChild = new List<SidebarMenu>()
                    {
                        MenuHelpers.AddModule("Matrices", "/ReportesMatrices/", "fa fa-circle-o text-red"),
                        MenuHelpers.AddModule("Diccionarios", "/ReportesDiccionarios/", "fa fa-circle-o text-red"),
                    };

                sesion.TreeChild = new List<SidebarMenu>()
                {
                    MenuHelpers.AddModule("Registra Usuario", "/Identity/Account/Register", "fa fa-user-secret"),
                    MenuHelpers.AddModule("Cierra Sesion", "/Identity/Account/Logout", "fa fa-sign-out"),
                };
            }
            else
            {
                procesos.TreeChild = new List<SidebarMenu>()
                    {
                        MenuHelpers.AddModule("Area Tramites", "/AreaTramite/Index", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("Estado financiacion", "/EstadoFinanciacion/Index", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("Plan Camil", "/PlanCamil/Index", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("LeyFondef", "/LeyFondef/Index", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("Articulo Ley", "/ArticuloLey/Index", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("Fuente Financiamiento", "/FuenteFinanciamiento/Index", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("Programas", "/Programas/Index", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("SubProgramas", "/SubProgramas/Index", "fa fa-circle-o text-aqua"),
                        MenuHelpers.AddModule("Estado", "/Estado/Index", "fa fa-circle-o text-aqua")
                    };

                gestion.TreeChild = new List<SidebarMenu>()
                    {
                        MenuHelpers.AddModule("Bapin", "/Bapin/Index", "fa fa-circle-o text-yellow"),
                    };

                reportes.TreeChild = new List<SidebarMenu>()
                    {
                        MenuHelpers.AddModule("Matrices", "/ReportesMatrices/", "fa fa-circle-o text-red"),
                        MenuHelpers.AddModule("Diccionarios", "/ReportesDiccionarios/", "fa fa-circle-o text-red"),
                    };

                sesion.TreeChild = new List<SidebarMenu>()
                {
                    MenuHelpers.AddModule("Registra Usuario", "/Identity/Account/Register", "fa fa-user-secret"),
                    MenuHelpers.AddModule("Cierra Sesion", "/Identity/Account/Logout", "fa fa-sign-out"),
                };
            }
            sidebars.Add(procesos);
            sidebars.Add(gestion);
            sidebars.Add(reportes);
            sidebars.Add(sesion);
            return View("LayoutSidebar", sidebars);

        }
    }
}
